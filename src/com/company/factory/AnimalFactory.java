package com.company.factory;

import com.company.util.AnimalType;
import com.company.animals.Animal;
import com.company.animals.Cat;
import com.company.animals.Dog;

public class AnimalFactory {

    public static Animal getAnimal(int select) {
        AnimalType selectedAnimal = AnimalType.fromValue(select);
        switch (selectedAnimal) {
            case DOG:
                return new Dog();
            case CAT:
                return new Cat();
            default:
                throw new IllegalArgumentException("Unrecognizable animal");
        }
    }

}
