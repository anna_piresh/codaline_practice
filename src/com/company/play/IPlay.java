package com.company.play;

import com.company.util.exceptions.HeartLegException;

public interface IPlay {

    void play() throws HeartLegException;

}
