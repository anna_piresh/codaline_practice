package com.company.animals;

import com.company.play.IPlay;

public interface IAnimal extends IPlay {

    void eat();
    void sleep(int delay);

}
