package com.company.animals;

import com.company.play.Toy;
import com.company.util.exceptions.HeartLegException;

public class Dog extends Animal {

    private Toy toy;

    public Dog() {
        toy = new Toy();
    }

    @Override
    public void eat() {
        System.out.println("Dog eat");
    }

    @Override
    public void play() {
        toy.play();
    }
}
