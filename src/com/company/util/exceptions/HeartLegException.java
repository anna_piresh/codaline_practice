package com.company.util.exceptions;

public class HeartLegException extends Exception {

    public HeartLegException(String message) {
        super(message);
    }
}
