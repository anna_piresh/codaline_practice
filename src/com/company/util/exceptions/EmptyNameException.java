package com.company.util.exceptions;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException(String message) {
        super(message);
    }
}
