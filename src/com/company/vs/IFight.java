package com.company.vs;

public interface IFight extends AutoCloseable {

    void fight();

}
